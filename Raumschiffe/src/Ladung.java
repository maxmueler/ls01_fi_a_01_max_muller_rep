public class Ladung {

    //Attributes
    private String bezeichnung;

    private int menge;


    //constructor
    public Ladung(){
    }
    public Ladung(String bezeichnung, int menge){
        this.bezeichnung = bezeichnung;
        this.menge = menge;
    }


    //getter and setter
    public void setBezeichnung(String name){
        this.bezeichnung = name;
    }
    public String getBezeichnung(){
        return this.bezeichnung;
    }

    public int getMenge(){
        return menge;
    }
    public void setMenge(int menge){
        this.menge = menge;
    }
}

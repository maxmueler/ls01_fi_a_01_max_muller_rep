class Game{

    public static void main(String[] args){

        //create ships with load
        Raumschiff klingonen = new Raumschiff(1, 100, 100, 100, 100, 2,"IKS-Hegh'ta");
        klingonen.addLadung(new Ladung("Ferengi Schneckensaft", 200));
        klingonen.addLadung(new Ladung("Bat'leth Klingonen Schwert", 200));

        Raumschiff romulaner = new Raumschiff(2, 100, 100, 100, 100, 2, "IRW Khazara");
        romulaner.addLadung(new Ladung("Borg-Schrott", 5));
        romulaner.addLadung(new Ladung("Rote Materie", 2));
        romulaner.addLadung(new Ladung("Plasma-Waffe", 50));

        Raumschiff vulkanier = new Raumschiff(0, 80, 80, 50, 100, 5, "Ni'Var");
        vulkanier.addLadung(new Ladung("Forschungssonde", 35));
        vulkanier.addLadung(new Ladung("Photonentorpedo", 3));

        //Die Klingonen schießen mit dem Photonentorpedo einmal auf die Romulaner.
        klingonen.photonentorpedoSchiessen(romulaner);
        //Die Romulaner schießen mit der Phaserkanone zurück.
        romulaner.phaserkanoneSchiessen(klingonen);
        System.out.print("\n");
        //Die Vulkanier senden eine Nachricht an Alle “Gewalt ist nicht logisch”.
        vulkanier.nachrichtAnAlle("Gewalt ist nicht logisch");
        //Die Klingonen rufen den Zustand Ihres Raumschiffes ab und geben Ihr Ladungsverzeichnis aus
        klingonen.zustandRaumschiff();
        klingonen.ladungsverzeichnisAusgeben();
        System.out.print("\n");
        //Die Vulkanier sind sehr sicherheitsbewusst und setzen alle Androiden zur Aufwertung ihres Schiffes ein (für Experten).
        vulkanier.reparaturDurchfuehren(true, true, true ,200);
        //Die Vulkanier verladen Ihre Ladung “Photonentorpedos” in die Torpedoröhren Ihres Raumschiffes und räumen das Ladungsverzeichnis auf (für Experten).
        vulkanier.photonentorpedosLaden(200);
        vulkanier.ladungsverzeichnisAufräumen();
        System.out.print("\n");
        //Die Klingonen schießen mit zwei weiteren Photonentorpedo auf die Romulaner.
        klingonen.photonentorpedoSchiessen(romulaner);
        klingonen.photonentorpedoSchiessen(romulaner);
        //Die Klingonen, die Romulaner und die Vulkanier rufen jeweils den Zustand Ihres Raumschiffes ab und geben Ihr Ladungsverzeichnis aus.
        klingonen.zustandRaumschiff();
        klingonen.ladungsverzeichnisAusgeben();
        System.out.print("\n");
        romulaner.zustandRaumschiff();
        romulaner.ladungsverzeichnisAusgeben();
        System.out.print("\n");
        vulkanier.zustandRaumschiff();
        vulkanier.ladungsverzeichnisAusgeben();
        System.out.print("\n");
        //Geben Sie den broadcastKommunikator aus.
        System.out.println("Logbucheinträge :");
        for (int i = 0; i < klingonen.eintraegeLogbuchZurueckgeben().size(); i++){
            System.out.println("    " + (i + 1) + " : " +klingonen.eintraegeLogbuchZurueckgeben().get(i));
        }
    }
}

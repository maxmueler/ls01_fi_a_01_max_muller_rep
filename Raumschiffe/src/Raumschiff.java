import java.util.ArrayList;
import java.util.Random;

public class Raumschiff {

    //Attributes
    private int photonentorpedoAnzahl;
    private int energieversorgungInProzent;
    private int schildeInProzent;
    private int huelleInProzent;
    private int lebenserhaltungssystemeInProzent;
    private int androidenAnzahl;
    private String schiffsnahme;
    private static ArrayList<String> broadcastKommunikator = new ArrayList();
    private ArrayList<Ladung> ladungsverzeichnis = new ArrayList();


    //constructors
    public Raumschiff(){
    }
    public Raumschiff(int photonentorpedoAnzahl,int energieversorgungInProzent, int zustandSchildeInProzent,int zustandHuelleInProzent, int zustandLebenserhaltungssystemeInProzent, int anzahlDroiden,String schiffsname){
        this.photonentorpedoAnzahl = photonentorpedoAnzahl;
        this.energieversorgungInProzent = energieversorgungInProzent;
        this.schildeInProzent = zustandSchildeInProzent;
        this.huelleInProzent = zustandHuelleInProzent;
        this.lebenserhaltungssystemeInProzent = zustandLebenserhaltungssystemeInProzent;
        this.androidenAnzahl = anzahlDroiden;
        this.schiffsnahme = schiffsname;
    }


    //getter and setter methods
    public int getPhotonentorpedoAnzahl(){
        return this.photonentorpedoAnzahl;
    }
    public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahlNeu){
        this.photonentorpedoAnzahl = photonentorpedoAnzahlNeu;
    }

    public int getEnergieversorgungInProzent(){
        return this.energieversorgungInProzent;
    }
    public void setEnergieversorgungInProzent(int zustandEnergieversorgungInProzentNeu){
        this.energieversorgungInProzent = zustandEnergieversorgungInProzentNeu;
    }

    public int getSchildeInProzent(){
        return this.schildeInProzent;
    }
    public void setSchildeInProzent(int zustandSchildeInProzentNeu){
        this.schildeInProzent = zustandSchildeInProzentNeu;
    }

    public int getHuelleInProzent(){
        return this.huelleInProzent;
    }
    public void setHuelleInProzent(int zustandHuelleInProzentNeu){
        this.huelleInProzent = zustandHuelleInProzentNeu;
    }

    public int getLebenserhaltungssystemeInProzent(){
        return this.lebenserhaltungssystemeInProzent;
    }
    public void setLebenserhaltungssystemeInProzent(int zustandLebenserhaltungssystemeInProzentNeu){
        this.lebenserhaltungssystemeInProzent = zustandLebenserhaltungssystemeInProzentNeu;
    }

    public int getAndroidenAnzahl(){
        return androidenAnzahl;
    }
    public void setAndroidenAnzahl(int androidenAnzahl){
        this.androidenAnzahl = androidenAnzahl;
    }

    public String getSchiffsnahme(){
        return schiffsnahme;
    }
    public void setSchiffsnahme(String schiffsnahme){
        this.schiffsnahme = schiffsnahme;
    }


    //further methods:
    private void treffer(Raumschiff r){
        if (r.getSchildeInProzent() > 0){
            r.setSchildeInProzent(r.getSchildeInProzent() - 50);
        }else{
            r.setEnergieversorgungInProzent(r.getEnergieversorgungInProzent() -50);
            r.setHuelleInProzent(r.getHuelleInProzent() -50);
        }
        //r.setLebenserhaltungssystemeInProzent((r.getEnergieversorgungInProzent() + r.getSchildeInProzent() + r.getHuelleInProzent()) / 3 );
        if (r.getHuelleInProzent() <= 0){
            r.setLebenserhaltungssystemeInProzent(0);
        }
        if (r.getLebenserhaltungssystemeInProzent() <= 0){
            nachrichtAnAlle("Die Lebbenserhaltungssysteme von " + r.getSchiffsnahme() + "sind zerstört worden");
        }
        System.out.println(r.getSchiffsnahme() + " wurde getroffen!");
    }

    public void addLadung(Ladung neueLadung){
        this.ladungsverzeichnis.add(neueLadung);
    }

    public void photonentorpedoSchiessen(Raumschiff r){
        if (this.photonentorpedoAnzahl <= 0){
            nachrichtAnAlle("-=*Click*=-");
            return;
        }else{
            this.photonentorpedoAnzahl--;
            treffer(r);
            nachrichtAnAlle("Photonentorpedo Abgeschossen");
        }
    }

    public void photonentorpedosLaden(int AnzahlTorpedos){
        int swapped = 0;
        for (int i = 0; i < ladungsverzeichnis.size(); i++) {
            //System.out.println("a");
            if (ladungsverzeichnis.get(i).getBezeichnung() == "Photonentorpedo" && ladungsverzeichnis.get(i).getMenge() > 0 && swapped < AnzahlTorpedos){
                ladungsverzeichnis.get(i).setMenge(ladungsverzeichnis.get(i).getMenge() - 1);
                swapped++;
                i--;
            }
        }
        if (swapped > 0){
            this.photonentorpedoAnzahl += swapped;
            System.out.println(swapped + " Torpedo(s) wurden eingesetzt");
        }else{
            nachrichtAnAlle("-=*Click*=-");
            System.out.println("Keine Photonentorpedos gefunden!");
        }
    }

    public void phaserkanoneSchiessen(Raumschiff r){
        if (this.energieversorgungInProzent < 50){
            nachrichtAnAlle("-=*Click*=-");
            return;
        }else{
            this.energieversorgungInProzent -= 50;
            treffer(r);
            nachrichtAnAlle("Phaserkanone Abgeschossen");
        }
    }

    public void nachrichtAnAlle(String message){
        this.broadcastKommunikator.add(message);
    }

    public ArrayList<String> eintraegeLogbuchZurueckgeben(){
        return this.broadcastKommunikator;
    }

    public void reparaturDurchfuehren(boolean schutzschilde, boolean energieversorgung, boolean schiffshuelle, int anzahlDroiden){
        int strukturen = 0;
        if (schiffshuelle){strukturen++;}
        if (energieversorgung){strukturen++;}
        if (schutzschilde){strukturen++;}

        int usedAndroids = 0;
        if (anzahlDroiden > this.androidenAnzahl){
            usedAndroids = this.androidenAnzahl;
            this.setAndroidenAnzahl(0);
        }else{
            usedAndroids = anzahlDroiden;
            this.androidenAnzahl -= anzahlDroiden;
        }

        Random r = new Random();
        int heal = (r.nextInt(100)* usedAndroids)/strukturen;
        if (schiffshuelle){this.huelleInProzent += heal;}
        if (energieversorgung){this.energieversorgungInProzent += heal;}
        if (schutzschilde){this.schildeInProzent += heal;}
    }

    public void zustandRaumschiff(){
        System.out.println("Zustand von " + this.schiffsnahme + " :");
        System.out.println("    Photonentorpedos : " + this.photonentorpedoAnzahl);
        System.out.println("    Energieversorgung : " + this.energieversorgungInProzent + "%");
        System.out.println("    Schilde : " + this.schildeInProzent + "%");
        System.out.println("    Huelle : " + this.huelleInProzent + "%");
        System.out.println("    Lebenserhaltungssysteme : " + this.lebenserhaltungssystemeInProzent + "%");
        System.out.println("    Androiden : " + this.androidenAnzahl);
    }

    public void ladungsverzeichnisAusgeben(){
        System.out.println("Ladung von " + this.schiffsnahme + " :");
        for(int i = 0; i < ladungsverzeichnis.size(); i++){
            System.out.println("    " + ladungsverzeichnis.get(i).getBezeichnung() + " :  " + Integer.toString(ladungsverzeichnis.get(i).getMenge()));
        }
    }

    public void ladungsverzeichnisAufräumen(){
        for(int i = 0; i < ladungsverzeichnis.size(); i++){
            if (ladungsverzeichnis.get(i).getMenge() <= 0){
                ladungsverzeichnis.remove(i);
            }
        }
    }
}
